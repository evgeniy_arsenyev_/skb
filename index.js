const url = require('url'),
    http = require('http');

http.createServer(function (req, res) {
    const params = url.parse(req.url,true).query,
        a = parseInt(params.a, 10) || 0,
        b = parseInt(params.b, 10) || 0,
        sum = a + b;

    res.writeHead(200, {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "X-Requested-With, Content-Type, Accept",
    });
    res.end(sum.toString());
}).listen(3000, "127.0.0.1");